from django.urls import path
from accounts.views import user_login, logout_view, signup

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("logout/", logout_view, name="logout"),
    path("login/", user_login, name="login"),
]
